﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoDDDClean.Core.Entities;

namespace ProjetoDDDClean.Core.Interfaces
{
    public interface IClienteRepository : IRepositoryBase<Cliente>
    {
    }
}
