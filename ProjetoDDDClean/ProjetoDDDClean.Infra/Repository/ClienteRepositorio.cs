﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoDDDClean.Core.Entities;
using ProjetoDDDClean.Core.Interfaces;

namespace ProjetoDDDClean.Infra.Repository
{
    public class ClienteRepositorio : RepositoryBase<Cliente>, IClienteRepository
    {
    }
}
