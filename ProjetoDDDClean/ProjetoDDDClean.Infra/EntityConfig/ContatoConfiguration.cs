﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoDDDClean.Core.Entities;

namespace ProjetoDDDClean.Infra.EntityConfig
{
    public class ContatoConfiguration : EntityTypeConfiguration<Contato>
    {
        public ContatoConfiguration()
        {
            HasKey(c => c.ContatoId);

            Property(c => c.Nome).IsRequired().HasMaxLength(150);
            Property(c => c.Telefone).IsOptional().HasMaxLength(15);
            Property(c => c.Email).IsRequired();


        }
    }
}
