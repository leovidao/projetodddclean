﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ProjetoDDDClean.Core.Entities;
using ProjetoDDDClean.UI.ViewModels;

namespace ProjetoDDDClean.UI.AutoMapper
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<ClienteViewModel, Cliente>();
                //x.CreateMap<ProdutoViewModel, Produto>();
            });
        }
    }
}